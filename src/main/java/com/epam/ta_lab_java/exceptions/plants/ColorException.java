package com.epam.ta_lab_java.exceptions.plants;

public class ColorException extends Exception {
    public ColorException(String errorMessage){
        super(errorMessage);
    }

}
