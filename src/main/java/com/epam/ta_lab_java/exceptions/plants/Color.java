package com.epam.ta_lab_java.exceptions.plants;

public enum Color {
    RED, GREEN, YELLOW, BLUE, BLACK, WHITE
}
