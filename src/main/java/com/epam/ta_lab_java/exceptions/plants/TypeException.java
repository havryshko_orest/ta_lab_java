package com.epam.ta_lab_java.exceptions.plants;

public class TypeException extends Exception {
    public TypeException (String errorMessage){
        super(errorMessage);
    }
}
