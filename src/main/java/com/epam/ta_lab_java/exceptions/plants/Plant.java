package com.epam.ta_lab_java.exceptions.plants;

public class Plant {
    private int size;
    private Color color;
    private Type type;

    Plant(int size, Color color, Type type) {
        this.size = size;
        this.color = color;
        this.type = type;
    }

    int getSize() {
        return size;
    }

    Color getColor() {
        return color;
    }

    Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Plants{" +
                "size=" + size +
                ", color=" + color +
                ", type=" + type +
                '}';
    }
}
