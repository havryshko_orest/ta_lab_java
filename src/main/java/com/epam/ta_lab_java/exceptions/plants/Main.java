package com.epam.ta_lab_java.exceptions.plants;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Plant> listOfPlants = Arrays.asList(
                new Plant(100, Color.BLUE, Type.TREE),
                new Plant(200, Color.BLACK, Type.BUSH),
                new Plant(20, Color.RED, Type.FLOWER),
                new Plant(10, Color.GREEN, Type.GRASS),
                new Plant(80000, Color.WHITE, Type.HERB));

        for (Plant plant : listOfPlants) {
            try {
                System.out.println(plant);
                if ((plant.getColor().equals(Color.BLUE) && (plant.getType().equals(Type.TREE)))) {
                    throw new ColorException("Incorrect color");
                }
                if ((plant.getSize() > 40) && (plant.getType().equals(Type.HERB))) {
                    throw new TypeException("Incorrect type");
                }
            } catch (ColorException | TypeException e) {
                e.printStackTrace();
            }

        }


    }
}
