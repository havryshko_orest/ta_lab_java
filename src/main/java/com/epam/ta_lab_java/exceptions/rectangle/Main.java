package com.epam.ta_lab_java.exceptions.rectangle;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a and b:");
        try {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            System.out.println("The square of rectangle is: " + Rectangle.squareRectangle(a, b));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }


    }
}
