package com.epam.ta_lab_java.exceptions.rectangle;

class Rectangle {

    static int squareRectangle(int a, int b) {
        if ((a <= 0) || (b <= 0)) {
            throw new IllegalArgumentException("Input values should be positive");
        } else {
            return a*b;
        }
    }
}
