package com.epam.ta_lab_java.aircompany;

public class CargoPlane extends Airplane {

    public CargoPlane(String modelName, int speed, int cargoCapacity, int flightDistance) {
        super(modelName, speed, cargoCapacity, flightDistance);
    }
}
