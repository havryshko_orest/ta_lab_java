# Tasks:
### 1. OOP Basics
Авіакомпанія. Визначити ієрархію літаків. Створити авіакомпанію. Підрахувати загальну місткість і
вантажопідйомність. Здійснити сортування літаків компанії за дальністю польоту. Знайти літак у
компанії, що відповідає заданому діапазону параметрів споживання пального.

### 2. Exceptions

1. Create a method for calculating the area of a rectangle int
squareRectangle (int a, int b), which should throw an
exception if the user enters negative value. Input values a and b
from console. Check the squareRectangle method in the
method main. Check to input nonnumeric value.
2. Create a class Plants, which includes fields int size, Color color
and Type type, and constructor where these fields are initialized.
Color and type are Enum. Override the method toString( ).
Create classes ColorException and TypeException and
describe there all possible colors and types of plants. In the
method main create an array of five plants. Check to work your
exceptions.